private_rooms_server_cfg = {}

function private_rooms_server_cfg:new()
  local public = {}
    public.type = "SRM_MODULE_CFG" -- IMPORTANT!!!! Without this line SRM Core will NOT understand that your script is a configuration and will NOT load a module!
    public.object = {} -- Needed for core to access module. Pointer to module main class' object. You have to set it from the module's main class, after the object's creation.
    public.name = "private_rooms_server" -- internal name, uses in HTML. Have to follow the rules of variables naming.
	public.networkName = "private_rooms_server" --internal name, needed to use module via emitter
    public.displayName = "ROOMS" -- name appears on the screen
    public.enabled = true -- Or false if user doesn't want to use module
    public.renderOnScreen = false -- Always false here.
    public.renderOnPlayerScreen = false -- Always false here.
    public.networking = false -- If module use emitter/receiver or not
	
	public.screenVar = screen


  setmetatable(public, self)
  self.__index = self 
  return public
end

private_rooms_server_config = private_rooms_server_cfg:new()