private_rooms = {}

function private_rooms:new()
  local private = {}
    private.name = private_rooms_config.name
    private.displayName = private_rooms_config.displayName
	private.networkName = private_rooms_config.networkName
	private.switch = private_rooms_config.openingSwitchVar
	private.detector = private_rooms_config.detectorVar
	private.roomID = private_rooms_config.roomID


  local public = {}
    function public:receiveFromCore(senderModule, message)
      --do whatever you want
    end

    function public:getModuleName()
      return private.name
    end

    function public:getModuleDisplayName()
      return private.displayName
    end
	
	function public:getModuleNetworkName()
      return private.networkName
    end
	
	function public:openDoor(nickname)  
	  local query = SRM:retrieveFromDatabank('PR_RECORD:' .. private.roomID)
	  
	  if query ~= 'SRM:NO_KEY' and query ~= 'TIME_EXCEED' then             
		local obj = json.decode(query)
            
		if obj[1] == nickname then
		  private.switch.activate()
		end
	  elseif query == 'TIME_EXCEED' then
	    self:init()
	  end
	end
	
	function public:lockDoor()
	  private.switch.deactivate()
	end

    function public:init()
	  response = SRM:retrieveFromDatabank('PR_RECORD:' .. private.roomID)
	  
      if response == 'SRM:NO_KEY' or response == 'TIME_EXCEED' then
	    local obj = {system.getPlayerName(unit.getMasterPlayerId()), system.getTime()}
	
	    SRM:sendToDatabank('PR_RECORD:' .. private.roomID, json.encode(obj))
	
	    private.switch.activate()
  	  end
    end
  
  
  setmetatable(public, self)
  self.__index = self 
  return public
end

if private_rooms_config.enabled == true then
  private_rooms_obj = private_rooms:new()
  private_rooms_config.object = private_rooms_obj
end