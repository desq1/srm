flightPanel = {}

function flightPanel:new()
  local private = {}
    private.SRM_config = {
      name = "flightPanel",
      networkName = "flightPanel",
      displayName = "INFO",
      enabled = true,
      renderOnScreen = false,
      useAdditionalScreen = false,
      renderOnPlayerScreen = true,
      displayInMainWidget = false,
      placeValuesOnHUD = true,
      numbersOfHUDValues = {9, 10, 11, 12},
      networking = false,
      useStartupHook = false,
      useShutdownHook = false
    }

    private.coreVar = core or nil
    private.antigravVar = antigrav or nil
    private.gyroVar = gyro or nil
    
    private.HUDValuesBinds = {} -- inside a "function() end" construction return a function from private.HUDValuesAssemblers or any value 
      private.HUDValuesBinds[1] = function() return '' end
      private.HUDValuesBinds[2] = function() return '' end
      private.HUDValuesBinds[3] = function() return '' end
      private.HUDValuesBinds[4] = function() return '' end
      private.HUDValuesBinds[5] = function() return '' end
      private.HUDValuesBinds[6] = function() return '' end
      private.HUDValuesBinds[7] = function() return '' end
      private.HUDValuesBinds[8] = function() return '' end
      private.HUDValuesBinds[9] = function() return 'THR' end
      private.HUDValuesBinds[10] = function() return private.HUDValuesAssemblers:thrust() end
      private.HUDValuesBinds[11] = function() return private.HUDValuesAssemblers:speed() end
      private.HUDValuesBinds[12] = function() return 'SPD' end

    private.HUDValuesAssemblers = {}
      function private.HUDValuesAssemblers:thrust()
        return math.floor(unit.getAxisCommandValue(0) * 100)
      end

      function private.HUDValuesAssemblers:speed()
        local speedVector = private.coreVar.getWorldVelocity()

        return math.floor(math.sqrt(speedVector[1]^2 + speedVector[2]^2 + speedVector[3]^2) * 3.6)
      end


  local public = {}
    function public:getSRMConfiguration()
      return private.SRM_config
    end

    function public:getPlayerScreenHTML()
      return ''
    end

    function public:getHUDValue(numberOfValue)
      return private.HUDValuesBinds[numberOfValue]()
    end
    

  setmetatable(public, self)
  self.__index = self 
  return public
end

flightPanel_obj = flightPanel:new()