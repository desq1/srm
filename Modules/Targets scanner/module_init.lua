targets_scanner = {}

function targets_scanner:new()
  local private = {}
    private.SRM_config = {
      name = "targetsScanner",
      displayName = "Targets Scanner",
      enabled = true,
      renderOnScreen = false,
      useAdditionalScreen = false,
      renderOnPlayerScreen = true,
      displayInMainWidget = false,
      widgetCoords = {21.5, 18},
      widgetSize = {10, 5.5},
      networking = false,
      useStartupHook = true,
      useShutdownHook = false
    }

    private.coreVar = core
    private.radarVar = radar_1
    private.timerName = 'scanner'
    private.timerTime = 0.01
    private.switchStatusKey = 21334589437598320
    private.status = 'red' -- 'red' = disengaged, 'green' = engaged
    private.targetsCount = 0


  local public = {}
    public.seenTargetsData = {}

    function public:getSRMConfiguration()
      return private.SRM_config
    end

    function public:receiveFromCore(senderNickname, senderModule, message) end

    function public:getPlayerScreenHTML()
      return '<div class="' .. private.SRM_config.name .. '" style="position:fixed;left:' .. private.SRM_config.widgetCoords[1] .. 'vw;top:' .. private.SRM_config.widgetCoords[2] .. 'vh;width:' .. private.SRM_config.widgetSize[1] .. 'vw;height:' .. private.SRM_config.widgetSize[2] .. [[vh;border:5px solid #FF0000;opacity:0.75;background-color:rgba(0, 0, 0, 0.75);">
        <p class="targetsScannerText" style="color:]] .. private.status .. [[">&nbsp;Targets scanner</p>
        <p class="targetsScannerText">&nbsp;Targets found: ]] .. private.targetsCount .. [[</p>
        <style>.targetsScannerText{font-size: 1.6vh;line-height: 1.5vh}</style>
      </div>]]
    end
  
    function public:onPlayerScreenClick(x, y) end

    function public:runStartupHook()
      system.print('Targets Scanner: Ready.')
    end

    function public:switchStatus(key)
      if key == private.switchStatusKey then
        if private.status == 'red' then
          private.status = 'green'
          system.print('Targets scanner: Engaged.')
        else
          private.status = 'red'
          system.print('Targets scanner: Disengaged.')
        end
      end
    end

    function public:estimateTargetDirections()
      if private.status == 'green' then
        for id, target in pairs(public.seenTargetsData) do
          local distance = private.radarVar.getConstructDistance(id)

          if distance < public.seenTargetsData[id].distance then
            public.seenTargetsData[id].ticksUntilClosestPoint = public.seenTargetsData[id].ticksUntilClosestPoint + 1
            public.seenTargetsData[id].distance = distance
          elseif public.seenTargetsData[id].direction == '%direction%' then
            local nominalPath = math.sqrt(160000000000 - distance * distance)
            local realPath = 700000 * (public.seenTargetsData[id].ticksUntilClosestPoint / (1 / private.timerTime))

            if realPath > nominalPath then
              public.seenTargetsData[id].direction = 'Supposedly co-directional'
            elseif realPath < nominalPath then
              public.seenTargetsData[id].direction = 'Supposedly counter-directional'
            else
              public.seenTargetsData[id].direction = 'Supposedly static'
            end
          end
        end
      end
    end

    function public:onTargetSpotted(id)
      if private.status == 'green' then
        if private.radarVar.isConstructAbandoned(id) ~= 1 and private.radarVar.getConstructType(id) == 'dynamic' then
          private.targetsCount = private.targetsCount + 1

          public.seenTargetsData[id] = {
            seenAt = system.getWaypointFromPlayerPos(),
            distance = 400000,
            ticksUntilClosestPoint = 0,
            direction = '%direction%',
            name = private.radarVar.getConstructName(id) or '%name%',
            size = private.radarVar.getConstructCoreSize(id) or '%size%'
          }

          unit.setTimer(private.timerName, private.timerTime)
        end
      end
    end

    function public:onTargetLost(id)
      if private.status == 'green' and public.seenTargetsData[id] ~= nil then
        system.print('Targets scanner: Target #' .. private.targetsCount .. ' detected.')
        system.print('Name: "' .. public.seenTargetsData[id].name .. '"')
        system.print('Coresize: ' .. public.seenTargetsData[id].size)
        system.print('Vector: ' .. public.seenTargetsData[id].direction)
        system.print('Minimal distance: ' .. string.format("%.2f", math.floor(public.seenTargetsData[id].distance) / 1000))
        system.print('Seen at: ' .. public.seenTargetsData[id].seenAt)
        system.print('Lost at: ' .. system.getWaypointFromPlayerPos())

        public.seenTargetsData[id] = nil

        if #public.seenTargetsData == 0 then
          unit.stopTimer(private.timerName)
        end

        return 'success'
      else
        return 'abort: scanner disengaged'
      end
    end

  setmetatable(public, self)
  self.__index = self 
  return public
end

targets_scanner_obj = targets_scanner:new()