radar_marks_cfg = {}

function module_cfg:new()
  local public = {}
    public.type = "SRM_MODULE_CFG" -- IMPORTANT!!!! Without this line SRM Core will NOT understand that your script is a configuration and will NOT load a module!
    public.object = {} -- Needed for core to access module. Pointer to module main class' object. You have to set it from the module's main class, after the object's creation.
    public.name = "radarMarks" -- internal name, uses in HTML. Have to follow the rules of variables naming.
    public.displayName = "MARKS" -- name appears on the screen
    public.enabled = true -- Or false if user doesn't want to use module
    public.renderOnScreen = false -- Or false. This parameter defines if module will render on the screen or not
    public.renderOnPlayerScreen = true -- Same as public.renderOnScreen, but for player's screen
    public.networking = false -- If module use emitter/receiver or not


  setmetatable(public, self)
  self.__index = self 
  return public
end

radar_marks_config = module_cfg:new()