gunnerHUD = {}

function gunnerHUD:new()
  local private = {}
    private.SRM_config = {
      name = "gunnerHUD",
      networkName = "gunnerHUD",
      displayName = "GUNNER",
      enabled = true,
      renderOnScreen = false,
      useAdditionalScreen = false,
      renderOnPlayerScreen = true,
      displayInMainWidget = false,
      widgetCoords = {3, 5},
      widgetSize = {60, 60},
      placeValuesOnHUD = false,
      networking = false,
      useStartupHook = false,
      useShutdownHook = false
    }
    private.radarVar = radar_1
    private.weapons = weapon


  local public = {}
    function public:getSRMConfiguration()
      return private.SRM_config
    end

    function public:receiveFromCore(senderNickname, senderModule, message) end

    function public:getPlayerScreenHTML()
      local HTML_code = [[
        <div class="]] .. private.SRM_config.name .. [[" style="position:fixed;left:]] .. private.SRM_config.widgetCoords[1] .. [[vw;top:]] .. private.SRM_config.widgetCoords[2] .. [[vh;width:]] .. private.SRM_config.widgetSize[1] .. [[vw;height:]] .. private.SRM_config.widgetSize[2] .. [[vh">
          <div class="inner" style="margin-bottom:3vh; height:10vh; display:flex; align-items:center">
            <div style="height:50%; padding-top:1.4vh; padding-left:1vw; padding-right:1vw; border-right:2px solid #FF0000">HIT CHANCE:</div>
      ]]

      for _, value in pairs(private.weapons) do
          local info = json.decode(value.getData())
        
          if info.properties.hitProbability >= 0.75 then
            color = '#4CAF50'
          elseif info.properties.hitProbability >= 0.45 then
            color = '#FDD835'
          else
            color = '#FF0000'
          end
    
          HTML_code = HTML_code .. [[
            <div style="display:flex; flex-direction:column; justify-content:space-between; align-items:center; margin-left:0.5vw; margin-right:0.5vw;">
              <div style="height:70%; color:]] .. color .. [[">]] .. math.floor(info.properties.hitProbability * 100) .. [[%</div>
              <div style="height:30%">]] .. info.name .. [[</div>
            </div>
          ]]
      end

      local target = private.radarVar.getTargetId();

      construct = {
        distance = private.radarVar.getConstructDistance(target) or 0, 
        name = private.radarVar.getConstructName(target) or "unidentified", 
        info = {
          speed = private.radarVar.getConstructSpeed(target) or 0, 
          angularSpeed = private.radarVar.getConstructAngularSpeed(target) or 0, 
          radialSpeed = private.radarVar.getConstructRadialSpeed(target) or 0, 
          spaceEngines = private.radarVar.getConstructInfos(target)['spaceEngines'] or 0, 
          weapons = private.radarVar.getConstructInfos(target)['weapons'] or 0, 
          radars = private.radarVar.getConstructInfos(target)['radars'] or 0
        }
      }

      HTML_code = HTML_code .. [[
          </div>

          <div class="inner" style="height:15vh; width:30%; padding-left:0.2vw; padding-top:0.5vh; padding-right: 0.2vw; display:flex; flex-direction:column">
            <div style="margin-bottom:0.25vh">Current target: ]] .. construct.name .. [[</div>
            <div class="data"><div>Distance:</div><div>]] .. string.format("%.2f", math.floor(construct.distance) / 1000) .. [[ km</div></div>
            <div class="data"><div>Speed:</div><div>]] .. math.floor(construct.info.speed) * 3.6 .. [[ km/h</div></div>
            <div class="data"><div>Radial speed:</div><div>]] .. math.floor(construct.info.radialSpeed) * 3.6 .. [[ km/h</div></div>
            <div class="data"><div>Angular velocity:</div><div>]] .. string.format("%.2f", construct.info.angularSpeed) .. [[ deg/s</div></div>
            <div class="data"><div>Engines:</div><div>]] .. math.floor(construct.info.spaceEngines) * 100 .. [[%</div></div>
            <!--<div class="data"><div>Weapons:</div><div>]] .. math.floor(construct.info.weapons) * 100 .. [[%</div></div>-->
            <div class="data"><div>Radars:</div><div>]] .. math.floor(construct.info.radars) * 100 .. [[%</div></div>
          </div>
        </div>

        <style>
          .inner{width:100%; border:5px solid #FF0000; opacity:0.75; background-color:rgba(0, 0, 0, 0.75)}
          .data{display:flex; justify-content:space-between; margin-bottom:0.3vh}
          .inner div{font-size:1.5vh; color:#FFFFFF; line-height:1.4vh}
        </style>
      ]]

      return HTML_code
    end

    function public:onPlayerScreenClick(x, y) end

  setmetatable(public, self)
  self.__index = self 
  return public
end

gunnerHUD_obj = gunnerHUD:new()