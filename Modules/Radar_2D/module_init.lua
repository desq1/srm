radar_list = {}

function radar_list:new()
  local private = {}
    private.SRM_config = {
      name = "radar2D",
      displayName = "RADAR",
      enabled = true,
      renderOnScreen = true,
      useAdditionalScreen = false,
      renderOnPlayerScreen = false,
      displayInMainWidget = false,
      placeValuesOnHUD = false,
      networking = false,
      useStartupHook = false,
      useShutdownHook = false,
      --feel free to set your radar's variable name below
      radarVar = radar_1
    }

    private.page = 1
    private.mode = 'map'
    private.sorted = {}

    private.activeScreenRenderer = 1

    private.screenRenderers = {}
      private.screenRenderers[1] = function()
        local HTML_code = '<div class="radar-list-entry"><div class="radar-list-entry-property">Range</div><div class="radar-list-entry-property">Length</div><div class="radar-list-entry-property">Owner</div><div class="radar-list-entry-property">Name</div></div>'

        for i = 1 + 15 * (private.page - 1), #private.sorted do
          if i > private.page * 15 then
            break
          end

          local distance = private.sorted[i][2]
          local length = private.sorted[i][3]
          local owner = private.sorted[i][4]
          local name = private.sorted[i][5]
          local status = private.sorted[i][6]
          local color = ''

          if status == 'ally' then
            color = '#008000'
          elseif status == 'enemy' then
            color = '#8B0000'
          else
            color = '#000000'   
          end

          HTML_code = HTML_code .. '<style>#entry' .. i .. '{background-color:' .. color .. ';}</style><div class="radar-list-entry" id="entry' .. i .. '"><div class="radar-list-entry-property">' .. distance .. 'km</div><div class="radar-list-entry-property">' .. length .. ' m</div><div class="radar-list-entry-property">' .. string.sub(owner, 0, 20) .. '</div><div class="radar-list-entry-property">' .. string.sub(name, 0, 20) .. '</div></div>'
        end
        
        HTML_code = HTML_code .. '<style>.radar-list-entries {width: 65%;display: flex;flex-direction: column;align-items: flex-start;}.radar-list-entry {width: 100%;height: 5vh;display: flex;justify-content: space-around;border-bottom: 1px solid black;margin-bottom: 3px; color:#FFFFFF;border-color:#FFFFFF;}.radar-list-entry-property{width: 13vw;height: 5vh;text-align: center;line-height: 5vh;white-space: nowrap;}</style>'

        return HTML_code
      end

      private.screenRenderers[2] = function()
        local HTML_code = ''

        for i = 1 + 15 * (private.page - 1), #private.sorted do
          if i > private.page * 15 then
            break
          end

          local status = private.sorted[i][6]
          local position = radar.getConstructPos(private.sorted[i][1])
          local range = private.SRM_config.radarVar.getRange()
          local x = 25.5 + (position[1] / 1000) / (range / 53) / 2
          local y = 39.5 - (position[2] / 1000) / (range / 81) / 2

          if status == 'ally' then
            color = '#008000'
          elseif status == 'enemy' then
            color = '#8B0000'
          else
            color = '#FFFFFF'   
          end

          HTML_code = HTML_code .. '<div class="radar-list-entry" id="entry' .. i .. '"></div><style>#entry' .. i .. '{border:2px dashed ' .. color .. ';transform:translate(' .. x .. 'vw,' .. y .. 'vh);}</style>'

        end

        HTML_code = HTML_code .. '<style>.radar-list-entries {width: 65%; height: 81vh; border: 2px solid red; border-radius: 50%;margin-top:10vh;}.radar-list-entry{width:1.8vw;height:3vh;position:fixed;}</style>'
      

        return HTML_code
      end

    function private:pageUp()
      local radar = private.SRM_config.radarVar
      local entries = radar.getEntries()

      if private.page > 1 then
        private.page = private.page - 1
        SRM:updateScreen(radar_list_obj)
      end
    end

    function private:pageDown()
      local radar = private.SRM_config.radarVar
      local entries = radar.getEntries()

      if private.page < math.ceil(#private.sorted / 15) then
        private.page = private.page + 1
        SRM:updateScreen(radar_list_obj)
      end
    end

    function private:switchMode()
      if private.mode == 'table' then
        private.mode = 'map'
      elseif private.mode == 'map' then
        private.mode = 'table'
      end

      SRM:updateScreen(radar_list_obj)
    end


  local public = {}
    function public:receiveFromCore(senderModule, message)
      --no integrations yet
    end

    function public:getScreenHTML()
      local radar = private.SRM_config.radarVar
      
      local range = radar.getRange() / 1000
      local entries = radar.getEntries()
      local total = 0
      local allies = 0
      local enemies = 0
      local neutrals = 0
      private.sorted = {}

      local HTML_code = '<style>.radar-list-wrap {display: flex;justify-content: space-evenly;width: 80vw;aligh-items: flex-start;}</style><div class="radar-list-wrap"><div class="radar-list-entries">'
        
      for i = 1, #entries do
        local owner = system.getPlayerName(radar.getConstructOwner(entries[i]))
        local size = radar.getConstructSize(entries[i])
        local length = math.floor(size[2])

        if owner ~= system.getPlayerName(unit.getMasterPlayerId()) and length < 4097 then
          local position = radar.getConstructPos(entries[i])
          local distance = math.floor(math.sqrt(position[1]^2 + position[2]^2 + position[3]^2)) / 1000
          local name = radar.getConstructName(entries[i])
          local record = SRM:retrieveFromDatabank('SRM:PERSON_RECORD:' .. owner)
          
          
          if type(record) == 'table' then
            if record.status == 'ally' then
              allies = allies + 1
            elseif record.status == 'enemy' then
              enemies = enemies + 1
            end
          else
            neutrals = neutrals + 1
          end
                
          total = total + 1
          private.sorted[total] = {entries[i], distance, length, owner, name, status}
        end
      end
        
      table.sort(private.sorted, function(a, b)
  	    return a[2] < b[2]
      end)

      if private.mode == 'table' then
        private.activeScreenRenderer = 1  
      elseif private.mode == 'map' then
        private.activeScreenRenderer = 2
      end

      return HTML_code .. private.screenRenderers[private.activeScreenRenderer]() .. '</div><div class="radar-list-overall"><div class="info">Range: ' .. range .. 'km<br>Total singatures: ' .. total .. '<br>Total allies:' .. allies .. '<br>Total enemies:' .. enemies .. '<br>Total neutrals and undefined:' .. neutrals .. '</div><div class="buttons"><div class="pageChangers"><div class="pageUp button">&#8679;</div><div class="pageDown button">&#8681;</div></div><div class="switchMode button button-last">SWITCH MODE</div></div></div>' .. '</div><style>radar-list-overall {display:flex; flex-direction:column; align-items:flex-start; width: 30%; color:#FFFFFF;}.buttons{display:flex;flex-direction:row;align-items:center;justify-content:space-between;flex-basis:content;font-size:5vh;}.button{width:10vw;height:5vh;border:2px solid red;line-height:5vh;text-align:center;}.button-last{height:9.9vh;font-size:3.6vh;line-height:4.45vh;}.info{margin-bottom: 15px;}</style>'
    end

    function public:getSRMConfiguration()
      return private.SRM_config
    end

    function public:onScreenClick(x, y)
      if x > 74 and x <= 85 then
        if y > 10 and y <= 15 then
          private:pageUp()
        elseif y > 15 and y <= 20 then
          private:pageDown()
        end
      elseif x > 85 and x <= 95 then
        if y > 10 and y <= 20 then
          private:switchMode()
        end
      end
    end

  setmetatable(public, self)
  self.__index = self 
  return public
end

radar_list_obj = radar_list:new()