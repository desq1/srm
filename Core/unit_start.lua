json = require('dkjson')

SRMCore = {}

function SRMCore:new()
  local private = {}
    private.modules = {renderOnScreen = {}, renderOnPlayerScreen = {}, networking = {}, useStartupHook = {}, useShutdownHook = {}, all = {}}
    private.screen = screen_1
    private.emitter = emitter_1
    private.runtimeDatabank = databank_1
    private.storageDatabank = databank_2
    private.additionalScreens = {}
    private.basicScreenBeginning = '<div class="container"><div class="leftmenu"><div class="leftmenu-element">&#8679;</div><style>.container{width: 100vw;height: 100vh;background-color:#000000}.leftmenu{left:0;top:0;height:100%;width:20%;}.leftmenu-element{width:100%;height:10%;border:2px solid red;text-align:center;line-height:10vh;font-size:5vh;color:#FFFFFF;}</style>'
    private.basicScreenEnd = '<div class="leftmenu-element leftmenu-element-arrow-down">&#8681;</div></div></div><style>.leftmenu-element-arrow-down{bottom:0;position:absolute;width:20vw}</style>'
    private.welcomeScreen = '<div class="wrap"><img src="http://du-srm.space/logo.png" class="logo"><div class="developer">Delta Squadron</div><div class="version">Version 1.17</div></div><style>.logo{position:fixed;width:256px;height:256px;left:50%;top:50%;margin-left:-128px;margin-top:-128px;}.wrap{height:100vh;width:100vw;background-color:#000000;}.version,.developer{font-size:4vh;line-height:4vh;bottom:1%;color:#FFFFFF;position:fixed;}.version{right:1%;}.developer{left:1%;}</style>'
    private.activeScreenTab = {}
    private.modulesMenuID = 1
    private.modulesMenuPage = 1
    private.modulesScreenMenu = {}
    private.modulesMainWidgetMenu = {}
    private.ENC_private = 1
    private.ENC_public = 1
    private.notificationsList = {}
    private.activeMainWidgetTab = {}
    private.mainWidgetTabs = {}
    private.widgets = {}
    private.resolution = {1920, 1080} -- your monitor resolution, needed for HUD
    private.HUD = {
      enabled = true,
      notificationBackgroundColor = 'hsla(0, 0%, 0%, 0.85)',
      notificationHeadingColor = '#FFFFFF',
      notificationFontColor = '#cce1ee',
      cursorColor = '#FFFFFF',
      cursorEnabled = false} -- you can customize the HUD. If you want to use semi-transparent colors, you must do it with hsla(), not rgba()
    private.shutdownKey = 123214214 -- CHANGE THIS
    private.removeNotificationKey = 2131242314 -- CHANGE THIS
    private.cursorAccessKey = 346546355 -- CHANGE THIS
    private.startupHookKey = 43654556445 -- CHANGE THIS
    private.peopleRecords = {everyone = {status = 'ally', accessLevel = 2}} -- {nickname = {status = 'ally or enemy', accessLevel = 0}} where access means: nil or the var doesn't exist = you can't switch the board on, 0 = you have access to switch the board on, 1 = you are the member of the crew, 2 = you can be a commander (needed for "main" boards) 
    private.commander = '' -- do not write anything here, it will be replaced anyway
    private.user = system.getPlayerName(unit.getMasterPlayerId())
    private.boardType = 'main' -- 'main' or 'additional'.

  local userValid = 0

  for key, value in pairs(private.peopleRecords) do
    if key == private.user or key == 'everyone' and value.accessLevel >= 0 then
      userValid = 1

      if value.accessLevel == 2 then
        userValid = 2
      end

      break
    end
  end

  if userValid == 0 then
    unit.exit()
  end
    
    function private:getHUDValue(number)
      if private.HUD.values[number] ~= nil then
        return private.HUD.values[number]:getHUDValue(number)
      else
        return ''
      end
    end

    function private:encode(message, key)
      -- TO DO
    end

    function private:decode(encoded_msg)
      -- TO DO
    end

    function private:renderModulesMenu()
      local screenPart = ''
      local screenReady = private.basicScreenBeginning
      local i = ((private.modulesMenuPage - 1) * 8) + 1;
        
      for _, value in pairs(private.modules.renderOnScreen) do
        if i == (private.modulesMenuPage * 8) + 1 then 
          break 
        end

        private.modulesScreenMenu[i] = value
        
        local cfg = value:getSRMConfiguration()

        screenPart = '<div class="leftmenu-element leftmenu-element-'..cfg.name..'">'..cfg.displayName..'</div>'
        screenReady = screenReady..screenPart

        i = i + 1
      end

      screenReady = screenReady..private.basicScreenEnd

      private.screen.addContent(0, 0, screenReady)
    end

    function private:screenArrowUpClick()
      if private.modulesMenuPage > 1 then
        private.modulesMenuPage = private.modulesMenuPage - 1
        private:renderModulesMenu()
      end
    end

    function private:screenArrowDownClick()
      if #private.modulesScreenMenu > 8 * private.modulesMenuPage then
        private.modulesMenuPage = private.modulesMenuPage + 1
        private:renderModulesMenu()
      end
    end

    function private:screenModuleTabClick(y)
      if private.activeScreenTab[1] ~= nil then
        private.screen.deleteContent(private.activeScreenTab[1].key)
      else
        private.activeScreenTab[1] = {key = 0, value = {}}
      end

      if y > 10 and y <= 20 and private.modulesScreenMenu[1 * (private.modulesMenuPage - 1) + 1] ~= nil then -- Module 1
        private.activeScreenTab[1].key = private.screen.addContent(20, 0, private.modulesScreenMenu[1 * (private.modulesMenuPage - 1) + 1]:getScreenHTML())
        private.activeScreenTab[1].value = private.modulesScreenMenu[1 * (private.modulesMenuPage - 1) + 1]
      end

      if y > 20 and y <= 30 and private.modulesScreenMenu[2 * (private.modulesMenuPage - 1) + 2] ~= nil then -- Module 2
        private.activeScreenTab[1].key = private.screen.addContent(20, 0, private.modulesScreenMenu[2 * (private.modulesMenuPage - 1) + 2]:getScreenHTML())
        private.activeScreenTab[1].value = private.modulesScreenMenu[2 * (private.modulesMenuPage - 1) + 2]
      end

      if y > 30 and y <= 40 and private.modulesScreenMenu[3 * (private.modulesMenuPage - 1) + 3] ~= nil then -- Module 3
        private.activeScreenTab[1].key = private.screen.addContent(20, 0, private.modulesScreenMenu[3 * (private.modulesMenuPage - 1) + 3]:getScreenHTML())
        private.activeScreenTab[1].value = private.modulesScreenMenu[3 * (private.modulesMenuPage - 1) + 3]
      end

      if y > 40 and y <= 50 and private.modulesScreenMenu[4 * (private.modulesMenuPage - 1) + 4] ~= nil then -- Module 4
        private.activeScreenTab[1].key = private.screen.addContent(20, 0, private.modulesScreenMenu[4 * (private.modulesMenuPage - 1) + 4]:getScreenHTML())
        private.activeScreenTab[1].value = private.modulesScreenMenu[4 * (private.modulesMenuPage - 1) + 4]
      end

      if y > 50 and y <= 60 and private.modulesScreenMenu[5 * (private.modulesMenuPage - 1) + 5] ~= nil then -- Module 5
        private.activeScreenTab[1].key = private.screen.addContent(20, 0, private.modulesScreenMenu[5 * (private.modulesMenuPage - 1) + 5]:getScreenHTML())
        private.activeScreenTab[1].value = private.modulesScreenMenu[5 * (private.modulesMenuPage - 1) + 5]
      end

      if y > 60 and y <= 70 and private.modulesScreenMenu[6 * (private.modulesMenuPage - 1) + 6] ~= nil then -- Module 6
        private.activeScreenTab[1].key = private.screen.addContent(20, 0, private.modulesScreenMenu[6 * (private.modulesMenuPage - 1) + 6]:getScreenHTML())
        private.activeScreenTab[1].value = private.modulesScreenMenu[6 * (private.modulesMenuPage - 1) + 6]
      end

      if y > 70 and y <= 80 and private.modulesScreenMenu[7 * (private.modulesMenuPage - 1) + 7] ~= nil then -- Module 7
        private.activeScreenTab[1].key = private.screen.addContent(20, 0, private.modulesScreenMenu[7 * (private.modulesMenuPage - 1) + 7]:getScreenHTML())
        private.activeScreenTab[1].value = private.modulesScreenMenu[7 * (private.modulesMenuPage - 1) + 7]
      end

      if y > 80 and y <= 90 and private.modulesScreenMenu[8 * (private.modulesMenuPage - 1) + 8] ~= nil then -- Module 8
        private.activeScreenTab[1].key = private.screen.addContent(20, 0, private.modulesScreenMenu[8 * (private.modulesMenuPage - 1) + 8]:getScreenHTML())
        private.activeScreenTab[1].value = private.modulesScreenMenu[8 * (private.modulesMenuPage - 1) + 8]
      end
    end

    function private:mainWidgetArrowLeftClick()
      if private.HUD.mainWidgetPage > 1 then
        private.HUD.mainWidgetPage = private.HUD.mainWidgetPage - 1
        SRM:updatePlayerScreen()
      end
    end

    function private:mainWidgetArrowRightClick()
      if #private.modulesMainWidgetMenu > 4 * private.HUD.mainWidgetPage then
        private.HUD.mainWidgetPage = private.HUD.mainWidgetPage + 1
        SRM:updatePlayerScreen()
      end
    end

    function private:mainWidgetTabClick(x, y)
      if x >= 76.6 and x <= 81.5 then
        private.activeMainWidgetTab = private.mainWidgetTabs[1]
      elseif x >= 81.6 and x <= 86.5 then
        private.activeMainWidgetTab = private.mainWidgetTabs[2]
      elseif x >= 86.6 and x <= 91.5 then
        private.activeMainWidgetTab = private.mainWidgetTabs[3]
      elseif x >= 91.6 and x <= 96.5 then
        private.activeMainWidgetTab = private.mainWidgetTabs[4]
      end

      SRM:updatePlayerScreen()
    end

    function private:commonPlayerScreenClick(x, y)
      for _, value in pairs(private.widgets) do
        local cfg = value:getSRMConfiguration()

        if cfg.widgetCoords[1] <= x and x <= cfg.widgetCoords[1] + cfg.widgetSize[1] then
          if cfg.widgetCoords[2] <= y and y <= cfg.widgetCoords[2] + cfg.widgetSize[2] then
            value:onPlayerScreenClick(x, y)
            break
          end
        end
      end
    end


  local public = {}
    public.secondsSinceStart = 0
    public.awaitTimers = {}

    function public:await(seconds, callback) -- run a callback function in the given amount of seconds
      timer = {}
      timer.timeToCall = public.secondsSinceStart + seconds
      timer.callback = callback

      table.insert(public.awaitTimers, timer)
    end

    function public:getAdditionalScreens()
      return private.additionalScreens
    end

    function public:sendToDatabank(databank, key, message)
      if databank == 'RuntimeDatabank' then
        databank = private.runtimeDatabank
      else
        databank = private.storageDatabank
      end

      if databank ~= nil then
        local msgtype = type(message)

        if msgtype == 'string' then
          databank.setStringValue(key, message)
        elseif msgtype == 'number' then
          databank.setFloatValue(key, message)
        end
      else
        return 'SRM:NO_DATABANK'
      end  
    end

    function public:retrieveFromDatabank(databank, key)
      if databank == 'RuntimeDatabank' then
        databank = private.runtimeDatabank
      else
        databank = private.storageDatabank
      end

      if databank ~= nil then
        if databank.hasKey(key) == 1 then
          local status, value = pcall(databank.getStringValue, key)
        
          if status then
            return value
          else
            status, value = pcall(databank.getFloatValue, key)
          
            if status then
              return value
            else
              return databank.getIntValue(key)
            end
          end
        else
          return 'SRM:NO_KEY'
        end
      else
        return 'SRM:NO_DATABANK'
      end
    end

    function public:getPeopleRecords()
      return private.peopleRecords
    end

    function public:getCommander()
      return private.commander
    end

    function public:getBoardType()
      return private.boardType
    end

    function public:sendToModule(receiverModule, senderNickname, senderModule, message)
      receiverModule:receiveFromCore(senderNickname, senderModule, message)
    end

    function public:sendToChannel(channel, senderModule, receiverNickname, receiverModule, body) 
      local message = '{"sender": ["' .. private.user .. '", "' .. senderModule:getSRMConfiguration().networkName .. '"], "receiver": ["' .. receiverNickname .. '", "' .. receiverModule .. '"], "body": "' .. body .. '"}'

      private.emitter.send(channel, message)
    end

    function public:receiveFromChannel(channel, message) --this function calls by receiver when it has incoming message
      result, obj = pcall(json.decode, message)

      if result then
        for key, value in pairs(private.peopleRecords) do
          if obj.receiver[1] == key and value.accessLevel >= 1 and obj.receiver ~= private.user then
            self:sendToModule(private.modules.networking[obj.receiver[2]], obj.sender[1], obj.sender[2], obj.body)
          end
        end
      end
    end

    function public:updateScreen(mod) --give this method a module main class' object pointer
      if not mod:getSRMConfiguration().useAdditionalScreen then
        if private.activeScreenTab[1] ~= nil then
          if private.activeScreenTab[1].value:getSRMConfiguration().name == mod:getSRMConfiguration().name then
            private.screen.deleteContent(private.activeScreenTab[1].key)
            private.activeScreenTab[1].key = private.screen.addContent(20, 0, mod:getScreenHTML())
          end
        end
      else
        mod:getSRMConfiguration().additionalScreenVar.setHTML(mod:getScreenHTML()) 
      end
    end

    function public:updatePlayerScreen() --re-writes the player's screen
      if private.HUD.enabled then
        local screenPart = ''
        local screenReady = ''

        if private.HUD.cursorEnabled then
          screenReady = screenReady .. '<svg style="position:fixed;left:' .. system.getMousePosX() .. 'px;top:' .. system.getMousePosY() .. 'px;width:1.2vw;height:2.12vh;enable-background:new 0 0 512 512;" x="0px" y="0px" viewBox="0 0 512 512"><path style="stroke:' .. private.HUD.cursorColor .. ';stroke-width:30;" d="M502.977,210.683L20.978,1.243C15.328-1.213,8.751,0.038,4.394,4.394C0.038,8.751-1.212,15.328,1.243,20.979 l209.44,481.999c2.39,5.5,7.81,9.022,13.753,9.022c0.283,0,0.568-0.008,0.851-0.024c6.273-0.355,11.66-4.582,13.497-10.589 l44.912-146.847c10.389-33.97,36.873-60.454,70.844-70.843l146.846-44.912c6.007-1.838,10.234-7.225,10.589-13.497 C512.33,219.016,508.739,213.187,502.977,210.683z M345.767,255.008c-43.52,13.31-77.448,47.239-90.758,90.758l-33.018,107.957 L43.923,43.923l409.8,178.067L345.767,255.008z"/></svg>'      
        end

        for _, value in pairs(private.widgets) do
          screenReady = screenReady .. value:getPlayerScreenHTML()
        end

        local value = private.notificationsList[1]
          
        if value ~= nil then
          screenReady = screenReady .. '<div class="'..value.heading..'"><div class="heading">'..value.heading..'</div><div class="description">'..value.description..'</div></div><style>.'..value.heading..'{z-index:10;position:fixed;left:3vw;top:5.5vh;width:25vw;height:15vh;background-color:' .. private.HUD.notificationBackgroundColor .. ';}.heading{font-size:3vh;line-height:3vh;margin-bottom:2vh;margin-left:1vw;margin-top:1vh;color:' .. private.HUD.notificationHeadingColor .. ';}.description{font-size:1.8vh;line-height:1.8vh;margin-left:1vw;color:' .. private.HUD.notificationFontColor .. ';}</style>'
        end
        
        system.setScreen(screenReady)
      end
    end

    function public:onScreenClick(x, y, usedScreen) -- if usedScreen = nil, then the main screen was used
      x = x * 100
      y = y * 100

      if usedScreen == nil then
        if x <= 20 then
          if y > 0 and y <= 10 then -- Arrow up
            private:screenArrowUpClick()
          end

          if y > 10 and y <= 90 then -- Modules
            private:screenModuleTabClick(y)
          end

          if y > 90 and y <= 100 then -- Arrow down
            private:screenArrowDownClick()
          end
        else
          if private.activeScreenTab[1] ~= nil then
            private.activeScreenTab[1].value:onScreenClick(x, y)
          end
        end
      else
        for _, value in pairs(private.additionalScreens) do
          if value[1] == usedScreen then
            value[2]:onScreenClick(x, y)
          end
        end
      end
    end

    function public:onPlayerScreenClick(x, y)
      x = x / (private.resolution[1] / 100)
      y = y / (private.resolution[2] / 100)

      if x >= 74 and x <= 99 then
        if y >= 73 and y <= 75.5 then
          if x >= 74 and x <= 76.5 then
            private:mainWidgetArrowLeftClick()
          elseif x >= 96.6 and x <= 99 then
            private:mainWidgetArrowRightClick()
          else
            private:mainWidgetTabClick(x, y)
          end
        elseif y >= 76.6 and y <= 98 then
          private.activeMainWidgetTab:onPlayerScreenClick(x, y)
        else
          private:commonPlayerScreenClick(x, y)
        end
      else
        private:commonPlayerScreenClick(x, y)
      end
    end

    function public:makeNotification(moduleDisplayName, text)
      table.insert(private.notificationsList, #private.notificationsList + 1, {heading = moduleDisplayName, description = text, timestamp = system.getTime() + 10})
    
      self:updatePlayerScreen()
    end
    
    function public:removeNotification(key)
      if key == private.removeNotificationKey then
        table.remove(private.notificationsList, 1)
         
        for i = 1, #private.notificationsList - 1 do
          private.notificationsList[i] = private.notificationsList[i + 1]
        end
        
        self:updatePlayerScreen()
      end
    end
    
    function public:getNotificationsList()
      return private.notificationsList
    end

    function public:enableCursor(key)
      if key == private.cursorAccessKey and private.HUD.enabled then
        system.lockView(1)
        private.HUD.cursorEnabled = true
      end
    end

    function public:disableCursor(key)
      if key == private.cursorAccessKey and private.HUD.enabled then
        system.lockView(0)
        private.HUD.cursorEnabled = false
      end
    end

    function public:getModulesList()
      return private.modules.all
    end

    function public:runStartupHooks(key)
      if key == private.startupHookKey then
        for _, mod in pairs(private.modules.useStartupHook) do
          mod:runStartupHook()
        end
      end
    end

    function public:shutdown(key)
      if key == private.shutdownKey then
        if private.boardType == 'main' and private.runtimeDatabank ~= nil then
          private.runtimeDatabank.clear()
        end

        for _, mod in pairs(private.modules.useShutdownHook) do
          mod:runShutdownHook()
        end

        if private.screen ~= nil then
          private.screen.clear()
          private.screen.setHTML(private.welcomeScreen)
        end

        system.setScreen('')
        system.showScreen(0)
            
        for _, value in pairs(private.additionalScreens) do
          value[1].clear()
        end
      end
    end

  --LOADING MODULES
  for _, value in pairs(_G) do -- look for modules in GLOBALS
    if type(value) == "table" then
      result, config = pcall(value.getSRMConfiguration)

      if result then
        if config.enabled then -- check if the module is enabled
          private.modules.all[config.name] = value -- add module in global list

          if config.renderOnScreen == true then -- add module in list of screen users
            if config.useAdditionalScreen then
              table.insert(private.additionalScreens, {config.additionalScreenVar, value})
            else
              private.modules.renderOnScreen[config.name] = value
            end
          end

          if config.renderOnPlayerScreen == true then -- add module in list of player's screen users
            private.modules.renderOnPlayerScreen[config.name] = value
          end

          if config.networking == true then -- add module in list of emitter&receiver users
            private.modules.networking[config.networkName] = value
          end

          if config.useStartupHook == true then -- add module in list of startup hook users
            private.modules.useStartupHook[config.name] = value
          end

          if config.useShutdownHook == true then -- add module in list of shutdown hook users
            private.modules.useShutdownHook[config.name] = value
          end
        end
      end
    end
  end
  --END OF LOADING

  --SECURITY KEYS GENERATION
  
  -- TO DO

  --END OF SECURITY KEYS GENERATION

  for _, value in pairs(private.modules.renderOnPlayerScreen) do
    table.insert(private.widgets, value)
  end

  --SETTING COMMANDER
  if private.boardType == 'main' and userValid == 2 then
    private.commander = private.user
  end
  --END OF SETTING

  --ADDING PEOPLE STATUSES TO RUNTIME DATABANK
  if private.runtimeDatabank ~= nil and private.boardType == 'main' then
    public:sendToDatabank('RuntimeDatabank', 'SRM:COMMANDER',  private.commander)

    for key, value in pairs(private.peopleRecords) do
      public:sendToDatabank('RuntimeDatabank', 'SRM:PERSON_RECORD:' .. key, json.encode(value))
    end
  end
  --END OF ADDING

  if private.screen ~= nil then
  	private:renderModulesMenu() -- screen initialization
    private.screen.activate()
  end

  for _, value in pairs(private.additionalScreens) do
    value[1].activate()
  end

  unit.setTimer('HUD', 0.04)
  unit.setTimer('clock', 0.1)
  unit.hide()
  system.showScreen(1)

  private.activeMainWidgetTab = private.mainWidgetTabs[1]

  public:updatePlayerScreen()

  setmetatable(public, self)
  self.__index = self 
  return public
end

SRM = SRMCore:new()
SRM:runStartupHooks(43654556445)